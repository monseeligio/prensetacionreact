import React from 'react'
import web from '../assets/img/desarrollo.png'


function Body() {
  return (
    <>
    <section className="body">
  <div className="about">
    <img src={web} alt={"web"}/>

    <div className="about-info">
      <h2>Acerca de mí</h2>
      <div className="divider"></div>
      <p>
        Mi interés por el desarrollo web se despertó en 2021, cuando empecé a explorar desde lo más básico. Utilicé HTML y CSS para crear páginas web estáticas y desde entonces, he continuado adquiriendo nuevos conocimientos en el área. He trabajado en la creación de páginas web dinámicas y funcionales utilizando frameworks como Angular y Vue. Mi deseo de aprender y mejorar sigue en aumento, y estoy comprometido en la búsqueda constante de nuevos conocimientos para mejorar mi desarrollo como Frontend. Mi objetivo final es seguir creciendo en el mundo de la tecnología para poder ofrecer soluciones innovadoras y eficientes a través del desarrollo web.
      </p>
    </div>

  </div>
  
  <div className="portfolio">
    <div className="portfolio-heading">
      <h2>Mi portafolio</h2>
      <div className="divider"></div>
    </div>
    <div className="container">

      <div className="card">
        <div className="card__img">
        </div>
        <div className="card__title">
          <h2>Proyecto escolar</h2>
          <h5>"Portafolio de evidencias"</h5>
        </div>
        <div className="card__proyect-links">
          <a href="https://gitlab.com/monseeligio/portafolio">
            <i className="fab fa-gitlab"></i>
          </a>
        </div>
        <br/>
      </div>

      <div className="card">
        <div className="card__img">
        </div>
        <div className="card__title">
          <h2>Proyecto personal</h2>
          <h5>"API Pokedex"</h5>
        </div>
        <div className="card__proyect-links">
          <a href="https://monseeligio.github.io/PokedexLaunch/">
            <i className="fab fa-github"></i>
          </a>
        </div>
        <br/>
      </div>

      <div className="card">
        <div className="card__img">
        </div>
        <div className="card__title">
          <h2>Proyecto personal</h2>
          <h5>"Pagina web de orden de pasteles"</h5>
        </div>
        <div className="card__proyect-links">
          <a href="https://github.com/monseeligio/Vue">
            <i className="fab fa-gitlab"></i>
          </a>
        </div>
        <br/>
      </div>

      <div className="card">
        <div className="card__img">
        </div>
        <div className="card__title">
          <h2>Proyecto personal</h2>
          <h5>"Landing de Vacunación "</h5>
        </div>
        <div className="card__proyect-links">
          <a href="https://monseeligio.github.io/PaginaVacuna/">
            <i className="fab fa-github"></i>
          </a>
        </div>
        <br/>
      </div>

    </div>
  </div>
</section>

    </>
  )
}

export default Body