import React from 'react'
import monse2 from '../assets/img/monse2.jpeg'

function Encabezado() {
  return (
    <>
                <div className="wrapper">
              <header>
                <nav className="navbar">
                  <div className="brand">
                    <h2>M<span>E</span></h2>
                  </div>
                  <ul className="menu">
                    <li><a href="#"  className="active"></a></li>
                    <li><a href="#"  className="active"></a></li>
                    <li><a href="#"  className="active">BIENVENIDO</a></li>
                  </ul>
                </nav>
              </header>

              <main className="main">
                <div className="info-content">
                  <h1>Hola soy  <span>Monserrat  </span></h1>
                   <p>Estudiante de Ingeniería en Sistemas Computacionales en el Instituto Tecnológico de Cuautla. Tengo 21 años y me apasiona el desarrollo web, 
                     por lo que me he enfocado en aprender todo lo necesario para ser un profesional 
                     Full Stack. </p>
                </div>
                <img src={monse2} alt={"monse2"}/>
              </main>  
            </div>

    </>
  )
}

export default Encabezado