import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

import Encabezado from './Components/Encabezado'
import Body from './Components/Body'
import Pie from './Components/Pie'

function App() {
  const [count, setCount] = useState(0)
  //aqui va toda la logica 
  return (
    
    <>
    {/** {de esta manera se hacen comentarios} 
    <div className=''>
      <h1>Hola mundo</h1>
      <h1>Hola mundo error</h1>
    </div>

    <div className=''>
      <h1>holis</h1>
    </div>
    <img src="" alt="" />
    <h2>
      {1+25}
    </h2>

    {/** operador ternario 
      ? if   : else
    <h3>{'monserrat eligio ramos'.toUpperCase()}</h3>
    <EjemploComponente/>
    <ComponenteDos/>
    */}
    <Encabezado/>
    <Body/>
    <Pie/>
    
    </>
  )
}
export default App
